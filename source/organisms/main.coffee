class Atoms.Organism.Main extends Atoms.Organism.Article

  @scaffold "assets/scaffold/main.json"


  # -- Children bubble events --------------------------------------------------
  onGame1: (event, dispatcher, hierarchy...) ->
  	__.Article.Game.show "Game1"
    # Your code...

  onGame2: (event, dispatcher, hierarchy...) ->
    console.log "ongame2"
    # Your code...

new Atoms.Organism.Main()
