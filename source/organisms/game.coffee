class Atoms.Organism.Game extends Atoms.Organism.Article

  @scaffold "assets/scaffold/game.json"

  constructor: ->
    super 
    do @render    

  show: ->
    max = 10
    min = 1
    randomCard = Math.floor(Math.random() * (max - min) + min)
    principal = __.Entity.Card.findBy("id",randomCard)    

    responses = []
    responses.push randomCard    
    while responses.length < 4            
      randomNumber = Math.floor(Math.random() * (max - min) + min)
      included = randomNumber in responses
      if not included      
        responses.push randomNumber

    #shuffle      

    i = responses.length
    while --i > 0
      j = ~~(Math.random() * (i + 1)) # ~~ is a common optimization for Math.floor
      t = responses[j]
      responses[j] = responses[i]
      responses[i] = t

    cardRespond1 = __.Entity.Card.findBy("id",responses[0])
    cardRespond2 = __.Entity.Card.findBy("id",responses[1])
    cardRespond3 = __.Entity.Card.findBy("id",responses[2])
    cardRespond4 = __.Entity.Card.findBy("id",responses[3])    

    @game.divQuestion.question.refresh url: "assets/images/animals/"+principal.line()
    @game.divQuestion.question.id = principal.id    

    @game.divGroup1.response1.refresh url: "assets/images/animals/"+cardRespond1.mini()
    @game.divGroup1.response1.id =  cardRespond1.id

    @game.divGroup1.response2.refresh url: "assets/images/animals/"+cardRespond2.mini()
    @game.divGroup1.response2.id =  cardRespond2.id

    @game.divGroup2.response3.refresh url: "assets/images/animals/"+cardRespond3.mini()
    @game.divGroup2.response3.id =  cardRespond3.id

    @game.divGroup2.response4.refresh url: "assets/images/animals/"+cardRespond4.mini()
    @game.divGroup2.response4.id =  cardRespond4.id

    Atoms.Url.path "game/game"



  # -- Children bubble events --------------------------------------------------
  onResponse1: (event, dispatcher, hierarchy...) ->
    if dispatcher.id is @game.divQuestion.question.id    
      __.Article.Game.show "Game1"

    # Your code...

  onResponse2: (event, dispatcher, hierarchy...) ->
    if dispatcher.id is @game.divQuestion.question.id
      __.Article.Game.show "Game1"
    # Your code...

  onResponse3: (event, dispatcher, hierarchy...) ->
    if dispatcher.id is @game.divQuestion.question.id
      __.Article.Game.show "Game1"    
    # Your code...

  onResponse4: (event, dispatcher, hierarchy...) ->
    if dispatcher.id is @game.divQuestion.question.id
      __.Article.Game.show "Game1"
    # Your code...

new Atoms.Organism.Game()

