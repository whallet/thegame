"use strict"

Atoms.$ ->
  console.log "------------------------------------------------------------"
  console.log "Atoms v#{Atoms.version} (Atoms.App v#{Atoms.App.version})"
  console.log "------------------------------------------------------------"

  Atoms.Url.path "main/main"
  Appnima?.key = "null"

	__.Entity.Card.create id: 1, name: "Cebra"
	__.Entity.Card.create id: 2, name: "Cerdo"
	__.Entity.Card.create id: 3, name: "Cocodrilo"
	__.Entity.Card.create id: 4, name: "Elefante"
	__.Entity.Card.create id: 5, name: "Jirafa"
	__.Entity.Card.create id: 6, name: "Leon"
	__.Entity.Card.create id: 7, name: "Lobo"
	__.Entity.Card.create id: 8, name: "Mono"
	__.Entity.Card.create id: 9, name: "Rinoceronte"
	__.Entity.Card.create id: 10, name: "Serpiente"

