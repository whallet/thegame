"use strict"

class __.Entity.Card extends Atoms.Class.Entity

	@fields "id", "name"

	principal: () -> @name + ".jpg"
	line: () -> @name + "_line.jpg"
	mini: () -> @name + "_mini.jpg"